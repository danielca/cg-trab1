all: main.o intersects.o opengl.o
	g++ -o crossLines main.o intersects.o opengl.o -lGL -lGLU -lglut

intersects: intersects.h intersects.cpp variables.h
	g++ -c intersects.cpp

opengl: opengl.h opengl.cpp variables.h
	g++ -c opengl.cpp -lGL -lGLU -lglut

main: main.cpp variables.h
	g++ -c main.cpp -lGL -lGLU -lglut

clean:
	rm *.o crossLines