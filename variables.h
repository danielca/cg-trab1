/**
@file variables.h
Trabalho 1 de Computação Gráfica
@author Daniel Cardoso Assumpção
*/

#ifndef VARIABLES
#define VARIABLES

	using namespace std;
	typedef enum {INTERSECT=0, VECTOR=1} _type;

	/**
	@struct Point
	Struct that contains the coordinates x,y of a point, and the type of point it its.
	*/
	struct Point
	{
		double x, y;
		_type type; 
	};

	extern bool firstClick;

	extern vector<pair<Point,Point> > lines;
	extern pair<Point,Point> activeP;
	extern vector<Point> allPoints;

	extern float screenW, screenH;

#endif
