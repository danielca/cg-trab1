/**
@file intersects.h
Trabalho 1 de Computação Gráfica
@author Daniel Cardoso Assumpção
*/


#include<GL/glut.h>
#include<stdbool.h>
#include<unistd.h>
#include<vector>
#include<algorithm>
#include<iostream>

#include "variables.h"

using namespace std;

void checkIntersection(pair<Point,Point> line);
Point intersects(pair<Point,Point> l1, pair<Point,Point> l2);