/**
@file main.cpp
Trabalho 1 de Computação Gráfica
@author Daniel Cardoso Assumpção
*/


#include<GL/glut.h>
#include<stdbool.h>
#include<unistd.h>
#include<vector>
#include<algorithm>
#include<iostream>

#include  "intersects.h"
#include "opengl.h"
#include "variables.h"

using namespace std;
float screenW = 800, screenH = 600;
bool firstClick = true;


vector<pair<Point,Point> > lines; /*!< Data struct that has all the lines of the code */
pair<Point,Point> activeP;/*!< Variable that has the point that is not yet a line. Used to make the moveable temporary line */
vector<Point> allPoints;/*!< Data struct that has all the points of the code */

	/**
       *Main function
       * @fn int main(int argc, char **argv)
       * 
       * 
       */
int main(int argc, char **argv) {
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGBA);
	glutInitWindowPosition(100,100);
	glutInitWindowSize(screenW,screenH);
	glutCreateWindow("Trabalho 1 - CG");
	glutMouseFunc(getMouse);
	glutPassiveMotionFunc(mouseDrag);
	glutDisplayFunc(renderScene);
	glutIdleFunc(renderScene);
	glutReshapeFunc(changeSize);
	glutMainLoop();
	return 0;
}

