/**
@file intersects.cpp
Trabalho 1 de Computação Gráfica
@author Daniel Cardoso Assumpção
*/


#include  "intersects.h"
    /**
       * Iterates through the current lines on the screen and determinates if it has a intersection
       * @fn void checkIntersection(pair<Point,Point> line)
       * @param line New line which's to be the intersections verified.
       * 
       */
void checkIntersection(pair<Point,Point> line){
	for (int i = 0; i < lines.size(); i++) {
		Point intersection = intersects(line, lines[i]);
		if(intersection.type == INTERSECT){
			allPoints.push_back(intersection);
		}
	}
}

/**
       * Check if two lines intersect and return the intersection point
       * @fn Point intersects(pair<Point,Point> l1, pair<Point,Point> l2)
       * @param l1 Line
       * @param l2 Line
       * @return Intersection point
       */

Point intersects(pair<Point,Point> l1, pair<Point,Point> l2){
	
	Point vector1, vector2, intersect;

    vector1.x = l1.second.x - l1.first.x;     
    vector1.y = l1.second.y - l1.first.y;
    
    vector2.x = l2.second.x - l2.first.x;     
    vector2.y = l2.second.y - l2.first.y;

    float s, t;
    s = (-vector1.y * (l1.first.x - l2.first.x) + vector1.x * (l1.first.y - l2.first.y)) / (-vector2.x * vector1.y + vector1.x * vector2.y);
    t = ( vector2.x * (l1.first.y - l2.first.y) - vector2.y * (l1.first.x - l2.first.x)) / (-vector2.x * vector1.y + vector1.x * vector2.y);

    if (s >= 0 && s <= 1 && t >= 0 && t <= 1)
    {
            intersect.x = l1.first.x + (t * vector1.x);
            intersect.y = l1.first.y + (t * vector1.y);
            intersect.type = INTERSECT;
    }
    return intersect;
}
