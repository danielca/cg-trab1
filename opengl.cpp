/**
@file opengl.cpp
Trabalho 1 de Computação Gráfica
@author Daniel Cardoso Assumpção
*/


#include "opengl.h"
	/**
       * Get the position and events of the mouse. This function creates the lines.
       * @fn void getMouse (int button, int state, int x, int y)
       * @param button  Which button is receiving the event.
       * @param state Which event is occurring with the button.
       * @param x Position of the mouse
       * @param y Position of the mouse
       * 
       */
void getMouse (int button, int state, int x, int y){
	if (button == GLUT_LEFT_BUTTON && state == GLUT_DOWN) {
		if(firstClick){
			activeP.first.x = x;
			activeP.first.y = y;
			activeP.first.type = VECTOR;
			activeP.second.x = x;
			activeP.second.y = y;
			activeP.second.type = VECTOR;
			firstClick = false;
		}
		else{
			activeP.second.x = x;
			activeP.second.y = y;
			checkIntersection(activeP);
			allPoints.push_back(activeP.first);
			allPoints.push_back(activeP.second);
			lines.push_back(activeP);
			firstClick = true;
			
		}
	}
}

	/**
       * Get the position of the mouse.
       * @fn void mouseDrag(int x, int y)
       * @param x Position of the mouse
       * @param y Position of the mouse
       * 
       */
void mouseDrag(int x, int y){
	if(!firstClick){
		activeP.second.x = x;
		activeP.second.y = y;
	}
	glutPostRedisplay();
}
	/**
       * Render the scene, drawing the lines and the points.
       * @fn void renderScene(void)
       */
void renderScene(void) {
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glPushMatrix();
	glMatrixMode (GL_PROJECTION);
    gluOrtho2D (0.0, screenW, screenH, 0.0);
    glEnable(GL_POINT_SMOOTH);
    glEnable(GL_LINE_SMOOTH);

    for (int i = 0; i < lines.size(); i++) {
		glLineWidth(3.0);
		glBegin(GL_LINES);
			glColor3f(255.0, 255.0, 255.0);
			glVertex3f(lines[i].first.x, lines[i].first.y, 0.0);
			glVertex3f(lines[i].second.x, lines[i].second.y, 0.0);
		glEnd();
		
    }
    for (int i = 0; i < allPoints.size(); i++) {
    	glPointSize(5.0);
		glBegin(GL_POINTS);
			if(allPoints[i].type == INTERSECT) glColor3f(255.0, 0.0, 0);
			else glColor3f(0.0, 0.0, 255.0);
			glVertex3f(allPoints[i].x, allPoints[i].y, 0.0);
		glEnd();
    }


	if(!firstClick){
		glPointSize(5.0);
		glBegin(GL_POINTS);
			glColor3f(0.0, 0.0, 255.0);
			glVertex3f(activeP.first.x, activeP.first.y, 0.0);
		glEnd();

		glLineWidth(3.0);
		glBegin(GL_LINES);
			glColor3f(255.0, 255.0, 255.0);
			glVertex3f(activeP.first.x, activeP.first.y, 0.0);
			glVertex3f(activeP.second.x, activeP.second.y, 0.0);
		glEnd();

	}
	glPopMatrix();
	glutSwapBuffers();
	glutPostRedisplay();
	glFlush();
}

	/**
       * Resize the windows .
       * @fn void changeSize(int w, int h)
       * @param w New window width
       * @param h New window height
       * 
       */

void changeSize(int w, int h) {

	// Prevent a divide by zero, when window is too short
	// (you cant make a window of zero width).
	if(h == 0)
           h = 1;

	float ratio = 1.0* w / h;

	// Reset the coordinate system before modifying
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	
	// Set the viewport to be the entire window
	screenW = w;
	screenH = h;
    glViewport(0, 0, w, h);
}