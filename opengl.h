/**
@file opengl.h
Trabalho 1 de Computação Gráfica
@author Daniel Cardoso Assumpção
*/


#include<GL/glut.h>
#include<stdbool.h>
#include<unistd.h>
#include<vector>
#include<algorithm>
#include<iostream>

#include  "intersects.h"
#include "variables.h"


using namespace std;

void renderScene(void);
void getMouse (int button, int state, int x, int y);
void changeSize(int w, int h);
void mouseDrag(int x, int y);
